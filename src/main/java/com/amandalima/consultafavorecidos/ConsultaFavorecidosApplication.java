package com.amandalima.consultafavorecidos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConsultaFavorecidosApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConsultaFavorecidosApplication.class, args);
	}

}
