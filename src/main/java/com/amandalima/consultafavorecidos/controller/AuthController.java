package com.amandalima.consultafavorecidos.controller;

import com.amandalima.consultafavorecidos.dto.ClientDto;
import com.amandalima.consultafavorecidos.exception.ClientException;
import com.amandalima.consultafavorecidos.repository.model.Client;
import com.amandalima.consultafavorecidos.service.ClientService;
import com.amandalima.consultafavorecidos.util.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/auth")
public class AuthController {

    @Autowired
    private ClientService service;

    @PostMapping
    public ResponseEntity<ClientDto> authenticate(@RequestParam(name = "cpfCnpj", required = true) Long cpfCnpj) throws ClientException {

        Client client = service.getClientByCpfCnpj(cpfCnpj);
        ClientDto dto = new ClientDto(client.getName(), client.getCpfCnpj(), JwtUtil.generateToken(client));

        return ResponseEntity.ok(dto);

    }

}
