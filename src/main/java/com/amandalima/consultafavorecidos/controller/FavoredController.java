package com.amandalima.consultafavorecidos.controller;

import com.amandalima.consultafavorecidos.repository.model.Favored;
import com.amandalima.consultafavorecidos.repository.projection.FavoredProjection;
import com.amandalima.consultafavorecidos.service.FavoredService;
import com.amandalima.consultafavorecidos.util.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/favoreds")
public class FavoredController {

    @Autowired
    private FavoredService service;

    @GetMapping
    public ResponseEntity<List<FavoredProjection>> getFavoreds(@RequestHeader(name = "Authorization") String authorization,
                                                               @RequestParam(required = false, defaultValue = "") String filter) {

        return ResponseEntity.ok(service.getFavoreds(JwtUtil.getCpfCnpjByToken(authorization), filter));

    }

}
