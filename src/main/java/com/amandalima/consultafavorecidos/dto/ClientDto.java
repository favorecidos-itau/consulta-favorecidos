package com.amandalima.consultafavorecidos.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
public class ClientDto {

    private String name;
    private Long cpfCnpj;
    private String token;

}
