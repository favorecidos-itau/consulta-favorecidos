package com.amandalima.consultafavorecidos.exception;


import org.springframework.http.HttpStatus;

public class ClientException extends BaseException {

    public ClientException(String message, HttpStatus status) {
        super(message, status);
    }

}
