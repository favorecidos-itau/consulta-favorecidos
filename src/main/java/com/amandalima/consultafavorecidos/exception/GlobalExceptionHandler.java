package com.amandalima.consultafavorecidos.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.LocalDateTime;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler({ClientException.class, FavoredException.class})
    public ResponseEntity<ErrorDetails> handleBaseException(BaseException ex, WebRequest webRequest) {
        return ResponseEntity.status(ex.getStatus())
                .body(buildError(ex, webRequest));
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorDetails> handleGenericException(Exception ex, WebRequest request) {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(buildError(ex, HttpStatus.INTERNAL_SERVER_ERROR, request));
    }

    private ErrorDetails buildError(BaseException ex, WebRequest request) {
        return ErrorDetails.builder()
                .timestamp(LocalDateTime.now())
                .message(ex.getMessage())
                .details(ex.getDetails())
                .status(ex.getStatus())
                .statusCode(ex.getStatus().value())
                .type(ex.getClass().getSimpleName())
                .path(request.getDescription(false))
                .build();
    }

    private ErrorDetails buildError(Exception ex, HttpStatus status, WebRequest request) {
        return ErrorDetails.builder()
                .timestamp(LocalDateTime.now())
                .message(ex.getMessage())
                .status(status)
                .statusCode(status.value())
                .type(ex.getClass().getSimpleName())
                .path(request.getDescription(false))
                .build();
    }

}
