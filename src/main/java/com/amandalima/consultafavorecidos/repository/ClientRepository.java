package com.amandalima.consultafavorecidos.repository;

import com.amandalima.consultafavorecidos.repository.model.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ClientRepository extends JpaRepository<Client, Long> {


    Optional<Client> findFirstByCpfCnpj(Long cpfCnpj);
}
