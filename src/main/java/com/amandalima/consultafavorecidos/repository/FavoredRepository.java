package com.amandalima.consultafavorecidos.repository;

import com.amandalima.consultafavorecidos.repository.model.Favored;
import com.amandalima.consultafavorecidos.repository.projection.FavoredProjection;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.util.List;

@Repository
public interface FavoredRepository extends JpaRepository<Favored, BigInteger> {

    @Query(nativeQuery = true,
            value = "select f.id, c.id as accountId, c.agencia as agency, " +
                    "c.conta as accountNumber, c.banco as bank, cli.nome as name, cli.cpf_cnpj as cpfCnpj " +
                    "from favorecido f " +
                    "inner join conta c on c.id = f.conta_id " +
                    "inner join cliente cli on cli.cpf_cnpj = c.cliente_cpf_cnpj " +
                    "where f.cliente_cpf_cnpj = ?1 and c.cliente_cpf_cnpj = ?2")
    List<FavoredProjection> findAllByClientCpfCnpjAndAccountClientCpfCnpj(Long cpfCnpj, Long favoredCpfCnpj);

    @Query(nativeQuery = true,
            value = "select f.id, c.id as accountId, c.agencia as agency, " +
                    "c.conta as accountNumber, c.banco as bank, cli.nome as name, cli.cpf_cnpj as cpfCnpj " +
                    "from favorecido f " +
                    "inner join conta c on c.id = f.conta_id " +
                    "inner join cliente cli on cli.cpf_cnpj = c.cliente_cpf_cnpj " +
                    "where f.cliente_cpf_cnpj = ?1 and cli.nome like %?2%")
    List<FavoredProjection> findAllByClientCpfCnpjAndAccountClientNameLike(Long cpfCnpj, String name);

    @Query(nativeQuery = true,
            value = "select f.id, c.id as accountId, c.agencia as agency, " +
                    "c.conta as accountNumber, c.banco as bank, cli.nome as name, cli.cpf_cnpj as cpfCnpj " +
                    "from favorecido f " +
                    "inner join conta c on c.id = f.conta_id " +
                    "inner join cliente cli on cli.cpf_cnpj = c.cliente_cpf_cnpj " +
                    "where f.cliente_cpf_cnpj = ?1")
    List<FavoredProjection> findAllByClientCpfCnpj(Long cpfCnpj);
}
