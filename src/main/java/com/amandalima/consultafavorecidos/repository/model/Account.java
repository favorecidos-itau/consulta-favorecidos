package com.amandalima.consultafavorecidos.repository.model;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigInteger;

@Entity
@Table(name = "conta")
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Account implements Serializable {

    @Id
    private BigInteger id;

    @Column(name = "agencia", nullable = false)
    private Long agency;

    @Column(name = "conta", nullable = false)
    private Long accountNumber;

    @Column(name = "banco", nullable = false)
    private String bank;

    @ManyToOne
    @JoinColumn(name = "cliente_cpf_cnpj", referencedColumnName ="cpf_cnpj", nullable = false)
    private Client client;

}
