package com.amandalima.consultafavorecidos.repository.model;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigInteger;

@Entity
@Table(name = "favorecido")
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Favored implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private BigInteger id;

    @Getter(AccessLevel.NONE)
    @Column(name = "cliente_cpf_cnpj")
    private Long clientCpfCnpj;

    @ManyToOne
    @JoinColumn(name = "conta_id", referencedColumnName = "id", nullable = false)
    private Account account;

}
