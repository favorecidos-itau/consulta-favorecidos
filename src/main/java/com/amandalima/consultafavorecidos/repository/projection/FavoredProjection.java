package com.amandalima.consultafavorecidos.repository.projection;

import java.math.BigInteger;

public interface FavoredProjection {

    BigInteger getId();
    void setId(BigInteger id);
    BigInteger getAccountId();
    void setAccountId(BigInteger id);
    Long getAgency();
    void setAgency(Long agency);
    Long getAccountNumber();
    void setAccountNumber(Long accountNumber);
    String getBank();
    void setBank(String bank);
    String getName();
    void setName(String name);
    Long getCpfCnpj();
    void setCpfCnpj(Long cpfCnpj);

}
