package com.amandalima.consultafavorecidos.service;

import com.amandalima.consultafavorecidos.exception.ClientException;
import com.amandalima.consultafavorecidos.repository.ClientRepository;
import com.amandalima.consultafavorecidos.repository.model.Client;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class ClientService {

    @Autowired
    private ClientRepository repository;

    public Client getClientByCpfCnpj(Long cpfCnpj) throws ClientException {
        return repository.findFirstByCpfCnpj(cpfCnpj)
                .orElseThrow(() -> new ClientException("Cliente não encontrado!", HttpStatus.NOT_FOUND));
    }
}
