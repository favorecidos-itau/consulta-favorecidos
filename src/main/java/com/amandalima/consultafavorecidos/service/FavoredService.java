package com.amandalima.consultafavorecidos.service;

import com.amandalima.consultafavorecidos.repository.FavoredRepository;
import com.amandalima.consultafavorecidos.repository.model.Favored;
import com.amandalima.consultafavorecidos.repository.projection.FavoredProjection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FavoredService {
    
    @Autowired
    private FavoredRepository repository;

    public List<FavoredProjection> getFavoreds(Long cpfCnpj, String filter) {

        if(filter.isBlank()) {
            return repository.findAllByClientCpfCnpj(cpfCnpj);
        }

        if(filter.chars().allMatch(Character::isDigit)) {
            return repository.findAllByClientCpfCnpjAndAccountClientCpfCnpj(cpfCnpj, Long.valueOf(filter));
        }

        return repository.findAllByClientCpfCnpjAndAccountClientNameLike(cpfCnpj, filter);
    }
}
