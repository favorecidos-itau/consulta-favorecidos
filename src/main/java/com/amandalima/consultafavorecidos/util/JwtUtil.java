package com.amandalima.consultafavorecidos.util;

import com.amandalima.consultafavorecidos.repository.model.Client;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;

import javax.crypto.SecretKey;
import java.util.Date;

public class JwtUtil {


    static final long EXPIRATION_TIME = 860_000_000;
    static final String SECRET = "8f80fea19cea3d9e453942fbca6106c5ad42a03fbd7dc01dd15e11b2b1e5ce53ac2f63a745374ba868b02f5d3f7b564c29151fcc6a35e50901c2abbf585f41f2";

    public static String generateToken(Client client) {

        SecretKey key = Keys.hmacShaKeyFor(Decoders.BASE64.decode(SECRET));

        return Jwts.builder()
                .setSubject(client.getCpfCnpj().toString())
                .setExpiration(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
                .signWith(key)
                .compact();

    }

    public static Long getCpfCnpjByToken(String token) {

        SecretKey key = Keys.hmacShaKeyFor(Decoders.BASE64.decode(SECRET));

        return Long.valueOf(Jwts.parser()
                .setSigningKey(key)
                .parseClaimsJws(token)
                .getBody()
                .getSubject());

    }

}
