package com.amandalima.consultafavorecidos.controller;

import com.amandalima.consultafavorecidos.exception.ClientException;
import com.amandalima.consultafavorecidos.repository.model.Client;
import com.amandalima.consultafavorecidos.service.ClientService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(AuthController.class)
@ExtendWith(SpringExtension.class)
class AuthControllerTest {

    public static final String BASE_URL = "/auth";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ClientService service;

    @Test
    void shouldAuthenticateAndReturnToken() throws Exception {

        var cpfCnpj = 12345678912L;
        var client = new Client(1L, "Test", cpfCnpj );

        when(service.getClientByCpfCnpj(cpfCnpj)).thenReturn(client);

        var requestBuilder = MockMvcRequestBuilders.post(BASE_URL)
                .param("cpfCnpj", String.valueOf(cpfCnpj));

        mockMvc.perform(requestBuilder)
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("Test"))
                .andExpect(jsonPath("$.cpfCnpj").value(cpfCnpj))
                .andExpect(jsonPath("$.token").isNotEmpty());

    }

    @Test
    void shouldThrowErrorWhenClientIsNotFound() throws Exception {

        when(service.getClientByCpfCnpj(12345678912L))
                .thenThrow(new ClientException("Cliente não encontrado!", HttpStatus.NOT_FOUND));

        var requestBuilder = MockMvcRequestBuilders.post(BASE_URL)
                .param("cpfCnpj", String.valueOf(12345678912L));

        mockMvc.perform(requestBuilder)
                .andExpect(status().isNotFound());
    }

    @Test
    void shouldThrowErrorWhenCPfCnpjIsNotInformed() throws Exception {

        var requestBuilder = MockMvcRequestBuilders.post(BASE_URL);

        mockMvc.perform(requestBuilder)
                .andExpect(status().isBadRequest());
    }
}