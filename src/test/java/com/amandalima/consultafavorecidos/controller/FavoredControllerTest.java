package com.amandalima.consultafavorecidos.controller;

import com.amandalima.consultafavorecidos.repository.model.Account;
import com.amandalima.consultafavorecidos.repository.model.Client;
import com.amandalima.consultafavorecidos.repository.model.Favored;
import com.amandalima.consultafavorecidos.repository.projection.FavoredProjection;
import com.amandalima.consultafavorecidos.service.FavoredService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.projection.ProjectionFactory;
import org.springframework.data.projection.SpelAwareProxyProjectionFactory;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.math.BigInteger;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(FavoredController.class)
@ExtendWith(SpringExtension.class)
class FavoredControllerTest {

    public static final String BASE_URL = "/favoreds";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private FavoredService service;

    @Test
    void shouldGetFavoredWithOutFilter() throws Exception {

        var favoreds = mockFavoreds().stream()
                .map(this::toProjection)
                .collect(Collectors.toList());

        var requestBuilder = MockMvcRequestBuilders.get(BASE_URL)
                .header("Authorization", "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIxMDA3MDA1OTYzMiIsImV4cCI6MTYxMjI5MDA0MH0.aG1tP1Gk9N_Zxx7xE9ORgCom5roHFPJ12t26OfnkpIt1ekRc93rJ7EtYNRODs8c6XVpkoc4D8SNanxYkc8tVHA")
                .contentType(MediaType.APPLICATION_JSON);

        when(service.getFavoreds(10070059632L, "")).thenReturn(favoreds);

        mockMvc.perform(requestBuilder)
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.size()").value(3))
                .andExpect(jsonPath("$[0].id").value(favoreds.get(0).getId()))
                .andExpect(jsonPath("$[0].accountId").value(favoreds.get(0).getAccountId()))
                .andExpect(jsonPath("$[0].agency").value(favoreds.get(0).getAgency()))
                .andExpect(jsonPath("$[0].accountNumber").value(favoreds.get(0).getAccountNumber()))
                .andExpect(jsonPath("$[0].bank").value(favoreds.get(0).getBank()))
                .andExpect(jsonPath("$[0].name").value(favoreds.get(0).getName()))
                .andExpect(jsonPath("$[0].cpfCnpj").value(favoreds.get(0).getCpfCnpj()));

    }

    @Test
    void shouldReturnFavoredFilteredByCpfCnpj() throws Exception {

        var favoreds = mockFavoreds().stream()
                .filter(favored -> favored.getAccount().getClient().getCpfCnpj().equals(6584577L))
                .map(this::toProjection)
                .collect(Collectors.toList());

        var requestBuilder = MockMvcRequestBuilders.get(BASE_URL)
                .queryParam("filter", "6584577")
                .header("Authorization", "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIxMDA3MDA1OTYzMiIsImV4cCI6MTYxMjI5MDA0MH0.aG1tP1Gk9N_Zxx7xE9ORgCom5roHFPJ12t26OfnkpIt1ekRc93rJ7EtYNRODs8c6XVpkoc4D8SNanxYkc8tVHA")
                .contentType(MediaType.APPLICATION_JSON);

        when(service.getFavoreds(10070059632L, "6584577")).thenReturn(favoreds);

        mockMvc.perform(requestBuilder)
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.size()").value(2))
                .andExpect(jsonPath("$[0].cpfCnpj").value("6584577"))
                .andExpect(jsonPath("$[1].cpfCnpj").value("6584577"));

    }

    @Test
    void shouldReturnFavoredFilteredByName() throws Exception {

        var favoreds = mockFavoreds().stream()
                .filter(favored -> favored.getAccount().getClient().getName().equals("Favored Client 2"))
                .map(this::toProjection)
                .collect(Collectors.toList());

        var requestBuilder = MockMvcRequestBuilders.get(BASE_URL)
                .queryParam("filter", "Client 2")
                .header("Authorization", "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIxMDA3MDA1OTYzMiIsImV4cCI6MTYxMjI5MDA0MH0.aG1tP1Gk9N_Zxx7xE9ORgCom5roHFPJ12t26OfnkpIt1ekRc93rJ7EtYNRODs8c6XVpkoc4D8SNanxYkc8tVHA")
                .contentType(MediaType.APPLICATION_JSON);

        when(service.getFavoreds(10070059632L, "Client 2")).thenReturn(favoreds);

        mockMvc.perform(requestBuilder)
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.size()").value(1))
                .andExpect(jsonPath("$[0].name").value("Favored Client 2"));
    }

    @Test
    void shouldThrowErrorWhenAuthorizationHeaderIsMissing() throws Exception {

        var requestBuilder = MockMvcRequestBuilders.get(BASE_URL);

        mockMvc.perform(requestBuilder)
                .andExpect(status().isBadRequest());

    }

    private FavoredProjection toProjection(Favored favored) {

        ProjectionFactory factory = new SpelAwareProxyProjectionFactory();
        FavoredProjection projection = factory.createProjection(FavoredProjection.class);
        projection.setId(favored.getId());
        projection.setAccountId(favored.getAccount().getId());
        projection.setAgency(favored.getAccount().getAgency());
        projection.setAccountNumber(favored.getAccount().getAccountNumber());
        projection.setBank(favored.getAccount().getBank());
        projection.setCpfCnpj(favored.getAccount().getClient().getCpfCnpj());
        projection.setName(favored.getAccount().getClient().getName());

        return  projection;

    }

    private List<Favored> mockFavoreds() {

        var account1 = Account.builder()
                .id(BigInteger.ONE)
                .agency(234L)
                .accountNumber(123L)
                .bank("Itau")
                .client(Client.builder()
                        .id(2L)
                        .cpfCnpj(6584577L)
                        .name("Favored Client")
                        .build())
                .build();
        var account2 = Account.builder()
                .id(BigInteger.valueOf(2))
                .agency(2324L)
                .accountNumber(12223L)
                .bank("Bradesco")
                .client(Client.builder()
                        .id(2L)
                        .cpfCnpj(6584577L)
                        .name("Favored Client")
                        .build())
                .build();

        var account3 = Account.builder()
                .id(BigInteger.valueOf(3))
                .agency(23244L)
                .accountNumber(1253L)
                .bank("Itau")
                .client(Client.builder()
                        .id(5L)
                        .cpfCnpj(658457447L)
                        .name("Favored Client 2")
                        .build())
                .build();

        return List.of( new Favored(BigInteger.ONE, 10070059632L, account1),
                        new Favored(BigInteger.valueOf(2), 10070059632L, account2),
                        new Favored(BigInteger.valueOf(3), 10070059632L, account3)
                );

    }
}