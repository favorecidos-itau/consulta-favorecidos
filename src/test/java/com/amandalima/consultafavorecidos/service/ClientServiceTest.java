package com.amandalima.consultafavorecidos.service;

import com.amandalima.consultafavorecidos.exception.ClientException;
import com.amandalima.consultafavorecidos.repository.ClientRepository;
import com.amandalima.consultafavorecidos.repository.model.Client;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ClientServiceTest {

    @Mock
    private ClientRepository repository;

    @InjectMocks
    private ClientService service;

    @Test
    void shouldGetClientByCpfCnpj() throws ClientException {

        var cpfCnpj = 12345678912L;
        var client = new Client(1L, "Test", cpfCnpj);

        when(repository.findFirstByCpfCnpj(cpfCnpj)).thenReturn(Optional.of(client));

        var result = service.getClientByCpfCnpj(cpfCnpj);

        assertThat(result).usingRecursiveComparison().isEqualTo(client);
    }

    @Test
    void shouldThrowErrorWhenClientIsNotFound() {

        var cpfCnpj = 12345678912L;
        when(repository.findFirstByCpfCnpj(cpfCnpj)).thenReturn(Optional.empty());

        assertThatExceptionOfType(ClientException.class)
                .isThrownBy(() -> service.getClientByCpfCnpj(cpfCnpj))
                .withMessage("Cliente não encontrado!");

    }
}