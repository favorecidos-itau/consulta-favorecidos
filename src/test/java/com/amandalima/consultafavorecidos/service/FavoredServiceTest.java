package com.amandalima.consultafavorecidos.service;

import com.amandalima.consultafavorecidos.repository.FavoredRepository;
import com.amandalima.consultafavorecidos.repository.model.Account;
import com.amandalima.consultafavorecidos.repository.model.Client;
import com.amandalima.consultafavorecidos.repository.model.Favored;
import com.amandalima.consultafavorecidos.repository.projection.FavoredProjection;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.projection.ProjectionFactory;
import org.springframework.data.projection.SpelAwareProxyProjectionFactory;

import java.math.BigInteger;
import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class FavoredServiceTest {

    @Mock
    private FavoredRepository repository;

    @InjectMocks
    private FavoredService service;

    @Test
    void shouldGetFavoredListWithOutFilter() {

        var cpfCnpj = 12345678912L;
        var favoredList = mockFavoreds().stream()
                .map(this::toProjection)
                .collect(Collectors.toList());

        when(repository.findAllByClientCpfCnpj(cpfCnpj)).thenReturn(favoredList);

        var result = service.getFavoreds(cpfCnpj, "");

        assertThat(result.size()).isEqualTo(3);

        verify(repository, never()).findAllByClientCpfCnpjAndAccountClientCpfCnpj(anyLong(), anyLong());
        verify(repository, never()).findAllByClientCpfCnpjAndAccountClientNameLike(anyLong(), anyString());
    }

    @Test
    void shouldGetFavoredListFilteredByCpfCnpj() {

        var cpfCnpj = 12345678912L;
        var filter = "6584577";
        var favoredList = mockFavoreds().stream()
                .filter(item -> item.getAccount().getClient().getCpfCnpj().equals(6584577L))
                .map(this::toProjection)
                .collect(Collectors.toList());

        when(repository.findAllByClientCpfCnpjAndAccountClientCpfCnpj(cpfCnpj, Long.valueOf(filter))).thenReturn(favoredList);
        var result = service.getFavoreds(cpfCnpj, filter);

        assertThat(result.size()).isEqualTo(2);

        verify(repository, never()).findAllByClientCpfCnpj(anyLong());
        verify(repository, never()).findAllByClientCpfCnpjAndAccountClientNameLike(anyLong(), anyString());
    }

    @Test
    void shouldGetFavoredListFilteredByName() {

        var cpfCnpj = 12345678912L;
        var filter = "Client 2";
        var favoredList = mockFavoreds().stream()
                .filter(item -> item.getAccount().getClient().getName().equals("Favored Client 2"))
                .map(this::toProjection)
                .collect(Collectors.toList());

        when(repository.findAllByClientCpfCnpjAndAccountClientNameLike(cpfCnpj, filter)).thenReturn(favoredList);
        var result = service.getFavoreds(cpfCnpj, filter);

        assertThat(result.size()).isEqualTo(1);

        verify(repository, never()).findAllByClientCpfCnpj(anyLong());
        verify(repository, never()).findAllByClientCpfCnpjAndAccountClientCpfCnpj(anyLong(), anyLong());
    }

    private FavoredProjection toProjection(Favored favored) {

        ProjectionFactory factory = new SpelAwareProxyProjectionFactory();
        FavoredProjection projection = factory.createProjection(FavoredProjection.class);
        projection.setId(favored.getId());
        projection.setAccountId(favored.getAccount().getId());
        projection.setAgency(favored.getAccount().getAgency());
        projection.setAccountNumber(favored.getAccount().getAccountNumber());
        projection.setBank(favored.getAccount().getBank());
        projection.setCpfCnpj(favored.getAccount().getClient().getCpfCnpj());
        projection.setName(favored.getAccount().getClient().getName());

        return  projection;

    }

    private List<Favored> mockFavoreds() {

        var account1 = Account.builder()
                .id(BigInteger.ONE)
                .agency(234L)
                .accountNumber(123L)
                .bank("Itau")
                .client(Client.builder()
                        .id(2L)
                        .cpfCnpj(6584577L)
                        .name("Favored Client")
                        .build())
                .build();
        var account2 = Account.builder()
                .id(BigInteger.valueOf(2))
                .agency(2324L)
                .accountNumber(12223L)
                .bank("Bradesco")
                .client(Client.builder()
                        .id(2L)
                        .cpfCnpj(6584577L)
                        .name("Favored Client")
                        .build())
                .build();

        var account3 = Account.builder()
                .id(BigInteger.valueOf(3))
                .agency(23244L)
                .accountNumber(1253L)
                .bank("Itau")
                .client(Client.builder()
                        .id(5L)
                        .cpfCnpj(658457447L)
                        .name("Favored Client 2")
                        .build())
                .build();

        return List.of( new Favored(BigInteger.ONE, 10070059632L, account1),
                new Favored(BigInteger.valueOf(2), 10070059632L, account2),
                new Favored(BigInteger.valueOf(3), 10070059632L, account3)
        );

    }
}